# cnb

`cnb`是参考`github.com/cnblogs/cli`重新设计。一方面是模块化设计，简化解析过程，二是，想提高rust的实践，只能利用下班时间抽空弄。不能保证稳定更新。

此部分也是参考了部分`cargo`的设计思路，抽象统一输出接口`Shell`，任务队列`JobQueue`。

## cnblog open API

从Cnblogs的[OpenAPI](https://api.cnblogs.com/help)来说，API主要有以下几类:

1. Token: 认证
2. Users: 仅提供当前登录用户信息
3. Blogs: 博客的CURD及其评论的查看和增加，
4. Marks: 收藏的CURD
5. News: 新闻的查询，新闻评论的CURD
6. Statuses: 闪存CURD。
7. Questions: 问题相关操作
8. Edu: 班级相关
9. Articles: 知识库的查找。
10. Zzk: 找找看

子命令参考github的`gh`的子命令设计。
